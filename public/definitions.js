window.defs = {
  menuItems: [
    {
      name: 'suppliers',
      label: 'Fournisseurs',
      onclick() {
        window.setCurrentMenu('suppliers');
        window.initList('suppliers');
      },
    },
    {
      name: 'products',
      label: 'Produits',
      onclick() {
        window.setCurrentMenu('products');
        window.initRaw('<p>En construction</p>');
      },
    },
  ],

  lists: [
    {
      name: 'suppliers',
      pluralLabel: 'fournisseurs',
      singularLabel: 'fournisseur',
      headerTemplate: `
        <tr>
          <th>Nom du fournisseur</th>
          <th>Contact</th>
          <th>Téléphone</th>
          <th>Actions</th>
        </tr>`,
      rowTemplate(supplier) {
        return `
          <td>${supplier.name}</td>
          <td>${supplier.contact}</td>
          <td>${supplier.telephone}</td>`;
      },
    },
  ],

  forms: [
    {
      name: 'suppliers',
      singularLabel: 'fournisseur',
      html: `<div class="field">
            <label class="label">Nom du fournisseur</label>
            <div class="control">
                <input name="name" class="input" type="text" placeholder="Entrez le nom du fournisseur">
            </div>
        </div>

        <div class="field">
            <label class="label">Nom du contact</label>
            <div class="control">
                <input name="contact" class="input" type="text" placeholder="Entrez le nom du contact">
            </div>
        </div>

        <div class="field">
            <label class="label">Numéro de téléphone</label>
            <div class="control">
                <input name="telephone" class="input" type="text" placeholder="Entrez le numéro de téléphone">
            </div>
        </div>`,
      collectValues() {
        const name = document.querySelectorAll('input[name="name"]')[0].value;
        const contact = document.querySelectorAll('input[name="contact"]')[0].value;
        const telephone = document.querySelectorAll('input[name="telephone"]')[0].value;
        return { name, contact, telephone };
      },
      initValues(supplier) {
        const nameElement = document.querySelectorAll('input[name="name"]')[0];
        nameElement.value = supplier.name;
        const contactElement = document.querySelectorAll('input[name="contact"]')[0];
        contactElement.value = supplier.contact;
        const telephoneElement = document.querySelectorAll('input[name="telephone"]')[0];
        telephoneElement.value = supplier.telephone;
      },
    },
  ],
};
